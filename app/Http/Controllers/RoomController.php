<?php
namespace App\Http\Controllers;

use App\Property;
use App\RentPays;
use App\Room;
use App\TenantRoom;
use App\Tenants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image as Image;


class RoomController extends Controller
{
    public function listPropertyRooms($ownerid, $propertyid)
    {
        // $studeDetails  = DB::table('rent_roomdetails')->where('propertyid', $propertyid)->where('ownerid', $ownerid)
        // ->orderBy('roomnumber', 'asc')->get();
        //  return response()->json($studeDetails, 200);
        $data = array();
        $data1 = array();
        $propertyRooms = DB::table('rent_roomtenantdetails')
            ->join('rent_tenantdetails', 'rent_tenantdetails.tenantid', '=', 'rent_roomtenantdetails.tenantid')
            ->join('rent_roomdetails', 'rent_roomdetails.roomid', '=', 'rent_roomtenantdetails.roomid')
        // ->where('rent_roomdetails.ownerid', $ownerid)
            ->where('rent_roomdetails.propertyid', $propertyid)
            ->orderBy('roomnumber', 'ASC')
            ->get();

        foreach (json_decode($propertyRooms, true) as $key => $value1) {
            $tempData;
            $currentData = RentPays::select('payid', 'status', 'Month_Year')->where('roomid', '=', $value1['roomid'])->where('tenantId', '=', $value1['tenantid'])->where('Month_Year', '=', date('M-Y'))->first();
            $tempData = $value1;
            if ($tempData['images']) {
                $tempData['images'] = json_decode($tempData['images']);
            }
            $tempData['rentPay'] = $currentData;
            array_push($data, $tempData);
        }
        $vacantRoom = DB::table("rent_roomdetails")->select('*')
        // ->where('rent_roomdetails.ownerid', $ownerid)
            ->where('rent_roomdetails.propertyid', $propertyid)
            ->whereNOTIn('roomid', function ($query) {
                $query->select('roomid')->from('rent_roomtenantdetails');
            })
            ->orderBy('roomnumber', 'ASC')
            ->get();
        foreach (json_decode($vacantRoom, true) as $key => $value1) {
            $tempData;
            $tempData = $value1;
            if ($tempData['images']) {
                $tempData['images'] = json_decode($tempData['images']);
            }
            array_push($data1, $tempData);
        }
        $combinedData = array_merge($data, $data1);
        return response()->json($combinedData, 200);
    }

    public function listVacantRoom($propertyid)
    {
        $vacantRoom = DB::table("rent_roomdetails")->select('*')
        // ->where('rent_roomdetails.ownerid', $ownerid)
            ->where('rent_roomdetails.propertyid', $propertyid)
            ->whereNOTIn('roomid', function ($query) {
                $query->select('roomid')->from('rent_roomtenantdetails');
            })
            ->orderBy('roomnumber', 'ASC')
            ->get();
        return response()->json($vacantRoom, 200);
    }

    public function roomTypeList()
    {
        $roomTypes = DB::table('rent_roomtypes')->select('roomType_Name')->get();
        return response()->json($roomTypes, 200);
    }

    public function saveRoom(Request $request)
    {
        Room::create($request->all());
        return response()->json('created', 201);
    }

    public function vacantRoomList()
    {

        try {
            $filterData = array();
            $vacantList = DB::table('rent_vacantList')->get();
            $dataDetails;
            foreach (json_decode($vacantList, true) as $key => $value1) {
                $roomDetails = Room::where('roomid', $value1['roomid'])->first();
                $propertyDetails = Property::where('propertyid', $value1['propertyid'])->first();
                $ownerDetails = DB::table('rent_ownerdetails')->where('ownerid', $propertyDetails['ownerid'])->first();
                //Property Details
                $dataDetails['areaName'] = $propertyDetails['areaName'];
                $dataDetails['city'] = $propertyDetails['city'];
                $dataDetails['state'] = $propertyDetails['state'];
                $dataDetails['pincode'] = $propertyDetails['pincode'];
                $dataDetails['parking'] = $propertyDetails['parking'];
                //Room Details
                $dataDetails['roomfloor'] = $roomDetails['roomfloor'];
                $dataDetails['roomtype'] = $roomDetails['roomtype'];
                $dataDetails['purpose'] = $roomDetails['purpose'];
                $dataDetails['rent'] = $roomDetails['rent'];
                $dataDetails['security'] = $roomDetails['security'];
                $dataDetails['furnishedStatus'] = $roomDetails['furnishedStatus'];
                $dataDetails['availableFrom'] = $value1['availablefrom'];
                $dataDetails['id'] = $roomDetails['roomid'];
                $dataDetails['images'] = $roomDetails['images'];
                $dataDetails['BuildingNumber'] = $propertyDetails['houseNumber'];
                //Owner Details
                $dataDetails['ownerContact'] = $ownerDetails->mobilenumber;
                $dataDetails['ownerName'] = $ownerDetails->name;

                array_push($filterData, $dataDetails);
            }
            return response()->json($filterData, 200);
        } catch (Exception $e) {
            return response()->json('Error -> ' . $e, 500);
        }
    }

    public function rentStatus(Request $request, $userid, $propertyid, $payId)
    {
        try {
            $entryStatus = RentPays::find($payId);
            if ($entryStatus) {
                $entryStatus->status = 'Paid';
                $entryStatus->recd_amt = $request['rent'];
                $entryStatus->pay_date = date('Y-m-d');
                $entryStatus->save();
                $data = array();
                $data1 = array();
                $propertyRooms = DB::table('rent_roomtenantdetails')
                    ->join('rent_tenantdetails', 'rent_tenantdetails.tenantid', '=', 'rent_roomtenantdetails.tenantid')
                    ->join('rent_roomdetails', 'rent_roomdetails.roomid', '=', 'rent_roomtenantdetails.roomid')
                // ->where('rent_roomdetails.ownerid', $ownerid)
                    ->where('rent_roomdetails.propertyid', $propertyid)
                    ->orderBy('roomnumber', 'ASC')
                    ->get();

                foreach (json_decode($propertyRooms, true) as $key => $value1) {
                    $tempData;
                    $currentData = RentPays::select('payid', 'status', 'Month_Year')->where('roomid', '=', $value1['roomid'])->where('tenantId', '=', $value1['tenantid'])->where('Month_Year', '=', date('M-Y'))->first();
                    $tempData = $value1;
                    if ($tempData['images']) {
                        $tempData['images'] = json_decode($tempData['images']);
                    }
                    $tempData['rentPay'] = $currentData;
                    array_push($data, $tempData);
                }
                $vacantRoom = DB::table("rent_roomdetails")->select('*')
                // ->where('rent_roomdetails.ownerid', $ownerid)
                    ->where('rent_roomdetails.propertyid', $propertyid)
                    ->whereNOTIn('roomid', function ($query) {
                        $query->select('roomid')->from('rent_roomtenantdetails');
                    })
                    ->orderBy('roomnumber', 'ASC')
                    ->get();
                foreach (json_decode($vacantRoom, true) as $key => $value1) {
                    $tempData;
                    $tempData = $value1;
                    if ($tempData['images']) {
                        $tempData['images'] = json_decode($tempData['images']);
                    }
                    array_push($data1, $tempData);
                }
                $combinedData = array_merge($data, $data1);
                return response()->json($combinedData, 200);
            } else {
                return response()->json('Error', 500);
            }
        } catch (Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function createrent()
    {
        try {
            $data = TenantRoom::get();
            foreach (json_decode($data, true) as $key => $value1) {
                RentPays::firstOrCreate(
                    [
                        'roomid' => $value1['roomid'],
                        'tenantId' => $value1['tenantid'],
                        'status' => 'Pending',
                        'Month_Year' => date('M-Y'),
                        'pay_date' => null,
                        'recd_amt' => 0,
                    ]
                );
            }
            return response()->json('Created', 200);
        } catch (Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function payRentFromProfile($roomieed, $payId)
    {
        $entryStatus = RentPays::find($payId);
        if ($entryStatus) {
            $entryStatus->status = 'Paid';
            $entryStatus->pay_date = date('Y-m-d');
            $entryStatus->save();

            try {
                $getLinkedData = TenantRoom::where('roomid', '=', $roomid)->get();
                $roomDetails;
                $tenantDetails;
                $objData;
                $tenantData;
                $propertyDetails;
                $roomDetails = Room::where('roomid', '=', $roomid)->first();
                $propertyDetails = Property::where('propertyid', '=', $roomDetails->propertyid)->first();
                if (count($getLinkedData) == 1) {
                    $tenantDetails = Tenants::where('tenantid', '=', $getLinkedData[0]->tenantid)->first();
                    $objData['status'] = $getLinkedData[0]->status;
                }
                $objData['propertyid'] = $roomDetails->propertyid;
                $objData['roomid'] = $roomDetails->roomid;
                $objData['rent'] = $roomDetails->rent;
                $objData['security'] = $roomDetails->security;
                $objData['roomtype'] = $roomDetails->roomtype;
                $objData['roomnumber'] = $roomDetails->roomnumber;
                $objData['roomfloor'] = $roomDetails->roomfloor;
                $objData['balcony'] = $roomDetails->balcony;
                $objData['city'] = $propertyDetails->city;
                $objData['pincode'] = $propertyDetails->pincode;
                $objData['houseNumber'] = $propertyDetails->houseNumber;
                $objData['areaName'] = $propertyDetails->areaName;
                $objData['purpose'] = $roomDetails->purpose;
                $objData['kitchen'] = $roomDetails->kitchen;
                $objData['bathroom'] = $roomDetails->bathroom;
                $objData['electricityType'] = $roomDetails->electricitytype;
                $objData['meterType'] = $roomDetails->meterType;
                $objData['roomPictures'] = $roomDetails->images;
                if (count($getLinkedData) > 0) {
                    $tenantData['tenantid'] = $tenantDetails->tenantid;
                    $tenantData['gender'] = $tenantDetails->gender;
                    $tenantData['policeverificationStatus'] = $tenantDetails->policeverificationStatus;
                    $tenantData['policeverificationnumber'] = $tenantDetails->policeverificationnumber;
                    $tenantData['name'] = $tenantDetails->name;
                    $tenantData['profilePicture'] = $tenantDetails->profilePicture;

                    $rentPayHistory = RentPays::select('payid', 'status', 'Month_Year', 'pay_date')
                        ->where('roomid', '=', $roomDetails->roomid)->where('tenantId', '=', $tenantDetails->tenantid)->get();
                    $tenantData['rentHistory'] = $rentPayHistory;

                    $objData['tenant'] = $tenantData;
                }

                return response()->json($objData, 200);

            } catch (Exception $e) {
                return response()->json('error', 500);
            }
        }
    }

    public function getPreviousRent($roomid, $tenantid)
    {
        $prevMonth = date('M-Y', strtotime("last month"));
        // $lastAmount = RentPays::select('rent_amount')->where('roomid', '=', $roomid)->where('tenantId', '=', $tenantid)->where('Month_Year', '=', '')->first();
        return response()->json($prevMonth, 200);
    }

    public function updateRoom(Request $request, $roomId)
    {
        // $upload = $request->input('');
        try {
            Room::findOrFail($roomId)->update($request->except('images'));
            if ($request->input('images')) {
                $tmpImages = array();
                // $prevImages = array();
                foreach ($request->input('images') as $image) {
                    $uniqIdQ = uniqid();
                    $path = '/home/gangaschool/public_html/rent/images/' . $roomId . '/';
                    // $path = '/home/shine3/projects/rentApp/images/' . $roomId . '/';
                    $publicPath = 'http://rent.gangarent.in/images/' . $roomId . '/';
                    if (!is_dir($path)) {
                        mkdir($path);
                    }
                    $image_64 = $image; //your base64 encoded data
                    $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
                    $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
                    $image = str_replace($replace, '', $image_64);
                    $image = str_replace(' ', '+', $image);
                    $imageName = 'room-' . $uniqIdQ . '.' . $extension;
                    $putcontent = file_put_contents($path . $imageName, base64_decode($image));
                    if ($putcontent) {
                        $compressThisImg = $path . $imageName;
                        Image::make($compressThisImg)->resize(1024, 768)->save($compressThisImg);
                        array_push($tmpImages, $publicPath . $imageName);
                    }
                }
                $roomData = Room::findOrFail($roomId);
                if ($roomData['images'] !== null || $roomData['images'] !== '' || is_array($roomData['images'])) {
                    if (is_array($roomData['images'])) {
                        if (empty($roomData['images'])) {
                            // push new images
                            $prevImages = $tmpImages;
                        } else {
                            // add existing images and new images also
                            $prevImages = array_merge((array) $roomData->images, $tmpImages);
                        }
                    } else {
                        $prevImages = array_merge((array) $roomData->images, $tmpImages);
                    }
                } else {
                    // push new images
                    $prevImages = $tmpImages;
                }
                $roomData->update(['images' => $prevImages]);
            }
            return response()->json('Success', 200);
        } catch (Exception $e) {
            return response()->json('Error' . $e, 500);
        }
    }

    //Function to compress an image
    public function compress($source, $destination, $quality)
    {
        // $info = getimagesize($source);

        // if ($info['mime'] == 'image/jpeg') {
        //     $image = imagecreatefromjpeg($source);
        // } elseif ($info['mime'] == 'image/gif') {
        //     $image = imagecreatefromgif($source);
        // } elseif ($info['mime'] == 'image/png') {
        //     $image = imagecreatefrompng($source);
        // }

        // imagejpeg($source, $destination, $quality);

        return $destination;
    }

}
