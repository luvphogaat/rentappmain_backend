<?php
namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;

class PropertyController extends Controller
{
    function list($userId) {
        $studeDetails = DB::table('rent_propertydetails')
            ->where('ownerid', $userId)->get();
        //     return response()->json(Student::orderBy('class', 'desc')->get(), 200);
        return response()->json($studeDetails, 200);
    }

    public function listWithFloors($userId)
    {
        $data = DB::table('rent_propertydetails')->select('propertyid', 'addressName', 'floors')
            ->where('ownerid', $userId)->get();
        return response()->json($data, 200);
    }

    public function create($userId, Request $request)
    {
        $uuid = (string) Uuid::generate(4);

        $data = Property::create($request->except('picture'));
        // if ($data) {
        if ($request['picture']) {
            $uniqIdQ = date('YmdHis');
            $path = '/home/gangaschool/public_html/rent/images/' . $uuid . '/';
            $publicPath = 'http://rent.gangarent.in/images/' . $uuid . '/';
            if (!is_dir($path)) {
                mkdir($path);
            }
            $image_64 = $request['picture']; //your base64 encoded data
            $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
            $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
            $image = str_replace($replace, '', $image_64);
            $image = str_replace(' ', '+', $image);
            $imageName = 'property-' . $uniqIdQ . '.' . $extension;
            $putcontent = file_put_contents($path . $imageName, base64_decode($image));
            if ($putcontent) {
                // DB::table('rent_propertydetails')
                // ->where("rent_propertydetails.propertyid", '=',  $uuid)
                // ->limit(1)
                // ->update(['rent_propertydetails.picture'=> $publicPath . $imageName]);
                $data = DB::table('rent_propertydetails')->insert(
                    [
                        'propertyid' => $uuid,
                        'ownerid' => $request['ownerid'],
                        'houseNumber' => $request['houseNumber'],
                        'areaName' => $request['areaName'],
                        'city' => $request['city'],
                        'state' => $request['state'],
                        'country' => $request['country'],
                        'floors' => $request['floors'],
                        'parking' => $request['parking'],
                        'CCTV' => $request['CCTV'],
                        'pincode' => $request['pincode'],
                        'picture' => $publicPath . $imageName,
                    ]
                );
                return response()->json('Created', 201);
            }
        } else {
            $data = DB::table('rent_propertydetails')->insert(
                [
                    'propertyid' => $uuid,
                    'ownerid' => $request['ownerid'],
                    'houseNumber' => $request['houseNumber'],
                    'areaName' => $request['areaName'],
                    'city' => $request['city'],
                    'state' => $request['state'],
                    'country' => $request['country'],
                    'floors' => $request['floors'],
                    'parking' => $request['parking'],
                    'CCTV' => $request['CCTV'],
                    'pincode' => $request['pincode'],
                ]
            );
            return response()->json('Created', 201);
        }
        //    }
        //    else
        //       return response()->json('Error', 500);
        //   }
        // return response()->json($request, 200);
    }

    public function edit(Request $request, $propertyId)
    {
        try {
            $data = Property::findOrFail($propertyId);
            $data->update($request->except('picture'));
            return response()->json('updated', 200);
        } catch (Exception $e) {
            return response()->json('Error' . $e, 500);
        }
    }

    public function listSingle($userid, $propertyId)
    {
        $details = DB::table('rent_propertydetails')
            ->where('propertyid', $propertyId)->where('ownerid', $userid)->first();
        return response()->json($details, 200);
    }

    public function utility()
    {
        $utilList = DB::table('rent_utilityServices')->get();
        return response()->json($utilList, 200);
    }

}
