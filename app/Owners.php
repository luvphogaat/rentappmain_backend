<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Owners extends Model
{

    protected $table = 'rent_ownerdetails';

    protected $primaryKey = 'ownerid';

    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [
        'ownerid', 'name', 'aadharcard', 'fathername', 'permanentaddress', 'mobilenumber', 'alternativenumber', 'email', 'gender', 'dob', 'occupation_details', 'status', 'nextpay', 'photo', 'city', 'state', 'country', 'zipcode',
    ];

    /**

     * The attributes excluded from the model's JSON form.

     *

     * @var array

     */

    protected $hidden = [];

    public static function boot()
    {

        parent::boot();

        static::creating(function ($model) {

            $model->ownerid = (string) Uuid::generate(4);

        });

    }

    public function getIncrementing()
    {

        return false;

    }

    public function getKeyType()
    {

        return 'string';

    }

}
