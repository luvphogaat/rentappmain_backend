<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Room extends Model
{

    protected $table = 'rent_roomdetails';

    protected $primaryKey = 'roomid';

    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [
        'roomid', 'ownerid', 'propertyid', 'purpose', 'roomtype', 'roomnumber', 'roomfloor', 'balcony', 'kitchen', 'bathroom', 'rent', 'security', 'watercharges', 'electricityType', 'meterType', 'furnishedStatus', 'availableFrom', 'images',
    ];

    /**

     * The attributes excluded from the model's JSON form.

     *

     * @var array

     */

    protected $hidden = [];

    protected $casts = [
        'images' => 'array',
    ];

    public static function boot()
    {

        parent::boot();

        static::creating(function ($model) {

            $model->roomid = (string) Uuid::generate(4);

        });

    }

    public function getIncrementing()
    {

        return false;

    }

    public function getKeyType()
    {

        return 'string';

    }

}
