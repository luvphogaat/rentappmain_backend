<?php header('Access-Control-Allow-Origin: *'); ?>
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });
// Lumen (5.8.12) (Laravel Components 5.8.*)
// $router->group(['middleware' => 'cors'], function() use ($router) {
$router->group(['prefix' => 'api', 'middleware' => 'cors'], function () use ($router) {

    $router->post('token' , ['uses' => 'AuthController@token']);
    $router->post('signup' , ['uses' => 'AuthController@signup']);
    $router->post('refresh', ['uses' => 'AuthController@refreshToken']);
    $router->group([ 'middleware' => 'auth' ], function () use ($router) {


        $router->get('user/{id}/settings', ['uses' => 'AuthController@accountInfo']);
        //get all vacant room list
        $router->get('user/property/{propertyid}/rooms', ['uses' => 'RoomController@listVacantRoom']);

        //search the tenant based on Aadhar Number and Mobile Number
        // $router->get('/user', ['uses' => 'TenantController@search']);
        $router->get('/user/searchTenant', ['uses' => 'TenantController@search']);

        // get owners list
        // $router->get( 'user', [ 'uses' => 'PropertyController@list' ]);

        //get single owner details
        // $router->get( 'user/{userid}', [ 'uses' => 'PropertyController@list' ]);

        //get Tenant Details
        $router->get( 'user/{userid}/tenant', [ 'uses' => 'TenantController@tenantlist' ]);

        // get property list of specific owner 
        $router->get( 'user/{userid}/locations', [ 'uses' => 'PropertyController@list' ]);

        $router->get( 'user/{userid}/locations/{propertyId}', [ 'uses' => 'PropertyController@listSingle' ]);

        $router->get( 'user/{userid}/locations/all', [ 'uses' => 'PropertyController@listWithFloors' ]);

        $router->put( 'user/property/{propertyId}/edit', ['uses' => 'PropertyController@edit']);

        // get owner rooms name of specific propertt
        // $router->get('user/{userid}/locations/{locationid}', [ 'uses' => '' ]);

        // get rooms list of specific property of specific user
        $router->get('user/{userid}/locations/{locationid}/rooms', [ 'uses' => 'RoomController@listPropertyRooms' ]);

        // get room details of specific property of specific user
        // $router->get('user/{userid}/rooms/{roomid}', [ 'uses' => '' ]);

        // specific owner -> specific property -> specific room -> bills list
        $router->get('user/{userid}/rooms/{roomid}/bills', [ 'uses' => 'BillsController@list' ]);

        // specific owner -> specific property -> specific room -> specific bill
        // $router->get('user/{userid}/locations/{locationid}/bills/{billid}', [ 'uses' => 'BillsController@single' ]);

        // get bills for th specific room
        // $router->get('user/{userid}/room/{roomid}/bills', ['uses' => '']);

        //add new bill for the room 
        $router->post('user/{userid}/rooms/{roomid}/bill', ['uses' => 'BillsController@addNewBill']);


        $router->get('user/{userid}/room/{roomid}/tenant/active', ['uses' => 'TenantController@roomRecordWithTenant']);

        $router->post('user/{userid}/tenant/save', ['uses' => 'TenantController@saveTenant']);

        $router->post('user/{userid}/room/save', ['uses' => 'RoomController@saveRoom']);
        
        $router->get('roomTypes', ['uses' => 'RoomController@roomTypeList']);

        $router->post('user/tenant/{tenantid}/uploadTenantProfile', ['uses' => 'TenantController@uploadProfile']);

        $router->post('user/tenant/{tenantid}/uploadTenantProfile', ['uses' => 'TenantController@uploadProfile']);

        $router->post('user/room/{roomid}/tenant/{tenantid}/unlink', ['uses' => 'TenantController@unlinkTenant']);

        $router->post('user/linkTenant', ['uses' => 'TenantController@linkTenant']);

        $router->get('user/tenant/{tenantid}/profile', ['uses' => 'TenantController@profile']);

        $router->get('user/tenant/{tenantid}/profilePicture', ['uses' => 'TenantController@profilePicture']);

        $router->get('user/room/vacant', ['uses' => 'RoomController@vacantRoomList']);

        $router->get('getCountriesStates' , function () {

            // Read File

            $jsonString = file_get_contents(base_path('resources/json/countryandstates.json'));

            $data = json_decode($jsonString, true);

            return response()->json($data, 200);
        });

        $router->post('user/{userId}/property/create', ['uses' => 'PropertyController@create']);

        $router->get('user/{userId}/dashboard', ['uses' => 'TenantController@dashboard']);

        $router->get('user/utility', ['uses' => 'PropertyController@utility']);

        $router->post('user/{userid}/property/{propertyid}/room/rent/{payId}' , ['uses' => 'RoomController@rentStatus']);

        $router->get('user/rent/list/create', ['uses' => 'RoomController@createrent']);

        $router->get('user/room/{roomid}/single/rent/{payId}/status', ['uses' => 'RoomController@payRentFromProfile']);

        $router->get('user/room/{roomid}/tenant/{tenantid}/rent/previous', ['uses' => 'RoomController@getPreviousRent']);

        $router->post('user/{uid}/sendSMS', ['uses' => 'TenantController@sendsmsByOwner']);

        $router->post('user/room/{roomId}/edit', ['uses' => 'RoomController@updateRoom']);

    });

});
// });
// Route::fallback(function(){
//     return response()->json([
//         'message' => 'Page Not Found. If error persists, contact info@website.com'], 404);
// });
