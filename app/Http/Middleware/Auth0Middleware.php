<?php

namespace App\Http\Middleware;

use Auth0\SDK\JWTVerifier;
use Closure;

class Auth0Middleware
{

    /**

     * Run the request filter.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \Closure  $next

     * @return mixed

     */

    public function handle($request, Closure $next)
    {

        if (!$request->hasHeader('Authorization')) {

            return response()->json('Authorization Header not found', 401);

        }

        $token = $request->bearerToken();

        if ($request->header('Authorization') == null || $token == null) {

            return response()->json('No token provided', 401);

        }

//        $this->retrieveAndValidateToken($token);

        try {

            $verifier = new JWTVerifier([

                'supported_algs' => ['RS256'],

                'valid_audiences' => ['http://localhost:3000'],

                'authorized_iss' => ['https://dev-14j4eu-n.auth0.com/'],

            ]);

            $decoded = $verifier->verifyAndDecode($token);

        } catch (\Exception $e) {

//            return response()->json($e, 403);

            return response()->json(['message' => $e->getMessage()], 401);

        };

        return $next($request);

    }

//    public function retrieveAndValidateToken($token)

//    {

//

//    }

}
