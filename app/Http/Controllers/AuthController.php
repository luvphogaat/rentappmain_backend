<?php

namespace App\Http\Controllers;

use App\Owners;
use App\Tenants;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{

    public function token(Request $request)
    {

        // return response($request->username);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dev-14j4eu-n.auth0.com/oauth/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "client_id=mlWo45Nd1AEHLVmhBRImmNjTgaoDt4vb&client_secret=vCfJZrcXncYz_yEATgjMoOVAieLWiwHHGyCrCvSUniJgpylruCDzZci-9mXBzxoY&audience=http://localhost:3000&scope=openid%20email%20email%20offline_access&grant_type=password&username=" . $request->username . "&password=" . $request->password,
            CURLOPT_HTTPHEADER => array(
                "application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $res = json_decode($response, true);
            if (array_key_exists('error', $res)) {
                if ($res['error'] === 'invalid_grant' && $res['error_description'] === 'Wrong email or password.') {
                    return response()->json('Bad Credentials', 401);
                }
            } else {

                $loginCheck = $this->login($request->username);

                if (count($loginCheck) > 0) {
                    $data = array(
                        "access_token" => $res['access_token'],
                        "refresh_token" => $res['refresh_token'],
                        "expires_in" => $res['expires_in'],
                        "id" => $loginCheck[0]->id,
                        "plan" => $loginCheck[0]->plan,
                        "userType" => $loginCheck[0]->user_type,
                        "username" => $loginCheck[0]->username,
                        "name" => $loginCheck[0]->name,
                        "email" => $loginCheck[0]->email,

                    );
                    return response()->json($data, 200);
                } else {
                    return response()->json('User not found', 404);
                }
            }
        }
    }

    public function signup(Request $request)
    {
        $data = array(
            'username' => $request->username,
            'password' => $request->password,
            'client_id' => 'mlWo45Nd1AEHLVmhBRImmNjTgaoDt4vb',
            'name' => $request->name,
            'email' => $request->email,
            'user_type' => $request->registerAs,
            'client_uid' => $request->client_uid,
            'connection' => 'Username-Password-Authentication',
        );

        if ($request['registerAs'] == 'Tenant') {
            $plan = array('plan' => 'GR0');
            $data['user_metadata'] = $plan;
        } else if ($request['registerAs'] == 'Landlord') {
            $plan = array('plan' => 'GR500');
            $data['user_metadata'] = $plan;
        }
        $payload = json_encode($data, true);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dev-14j4eu-n.auth0.com/dbconnections/signup",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $payload,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $res = json_decode($response, true);
            if (array_key_exists('error', $res)) {
                if ($res['error']) {
                    return response()->json($res['error'], 401);
                }
            } else {
                try {
                    if (isset($res['_id'])) {
                        $authid = $res['_id'];
                        $request->request->add(['auth_id' => $authid]);
                        if ($request['registerAs'] == 'Tenant') {
                            $tenantDetailsPrefilled = Tenants::where('name', '=', $request['name'])
                                ->Where('fathername', '=', $request['fathername'])
                                ->Where('mobilenumber', '=', $request['mobilenumber'])
                                ->Where('aadharcard', '=', $request['aadharcard'])
                                ->orWhere('email', '=', $request['email'])
                                ->first();
                            if (count((array) $tenantDetailsPrefilled)) {
                                $request->request->add(['userId' => $tenantDetailsPrefilled['tenantid']]);
                                $request->request->add(['plan' => 'GR500']);
                                $request->request->add(['user_type' => $request['registerAs']]);
                                $data = User::create($request->all());
                                return response()->json('Created', 201);
                            } else {
                                $details = Tenants::create($request->all());
                                $request->request->add(['userId' => $details['tenantid']]);
                                $request->request->add(['plan' => 'GR500']);
                                $request->request->add(['user_type' => $request['registerAs']]);
                                $data = User::create($request->all());
                                return response()->json('Created', 201);
                            }
                        } else if ($request['registerAs'] == 'Landlord') {
                            $details = Owners::create($request->all());
                            $request->request->add(['userId' => $details['ownerid']]);
                            $request->request->add(['plan' => 'GR0']);
                            $request->request->add(['user_type' => $request['registerAs']]);
                            $data = User::create($request->all());
                            return response()->json('Created', 201);
                        }
                        // return response()->json('Created', 201);
                    } else {
                        return (new Response($res['description'], $res['statusCode']))
                            ->header('Content-Type', 'application/json; charset=utf-8');
                    }
                } catch (Exception $ex) {
                    return response()->json($ex);
                }
            }
        }
        //curl ending
    }

    public function login($username)
    {
        $auth = DB::table('users')->where('username', $username)->get();
        if ($auth) {
            return $auth;
        } else {
            return [];
        }
    }

    public function refreshToken(Request $request)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dev-14j4eu-n.auth0.com/oauth/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "client_id=mlWo45Nd1AEHLVmhBRImmNjTgaoDt4vb&client_secret=vCfJZrcXncYz_yEATgjMoOVAieLWiwHHGyCrCvSUniJgpylruCDzZci-9mXBzxoY&audience=http://localhost:3000&scope=openid%20email%20email%20offline_access&grant_type=refresh_token&refresh_token=" . $request->refresh,
            CURLOPT_HTTPHEADER => array(
                "application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            // return response(json_decode($response, true)['error'], 200);
            $res = json_decode($response, true);
            if (array_key_exists('error', $res)) {
                if ($res['error'] === 'invalid_grant' && $res['error_description'] === 'Wrong email or password.') {
                    return response()->json('Bad Credentials', 401);
                }
            } else {

                // $loginCheck = $this->login($request->username, $request->password);
                // return $loginCheck;
                // if ( count($loginCheck) > 0 ){
                //   $data = array(
                //   "access_token" => $res['access_token'],
                //   "refresh_token" => $res['refresh_token'],
                //   "expires_in" => $res['expires_in'],
                //   "id" => $loginCheck[0]->id,
                //   "plan" => $loginCheck[0]->plan,
                //   "userType" => $loginCheck[0]->user_type

                // );
                //   return response()->json($data, 200);
                // } else {
                //   return response()->json('User not found', 404);
                // }
                $loginCheck = DB::table('users')->where('id', $request->id)->get();
                if (count($loginCheck) > 0) {
                    $data = array(
                        "access_token" => $res['access_token'],
                        "refresh_token" => $res['refresh_token'],
                        "expires_in" => $res['expires_in'],
                        "id" => $loginCheck[0]->id,
                        "plan" => $loginCheck[0]->plan,
                        "userType" => $loginCheck[0]->user_type,

                    );
                    return response()->json($data, 200);
                }

            }
        }
    }

    public function accountInfo($id)
    {
        $loginId = User::where('id', '=', $id)->first();
        $userType = $loginId->user_type;
        $userId = $loginId->userId;
        if ($userId) {
            if ($userType == 'Landlord') {
                try {
                    $userData = DB::table('rent_ownerdetails')->where('ownerid', $userId)->first();
                    $userData->id = $userData->ownerid;
                    return response()->json($userData, 200);
                } catch (Exception $e) {
                    return response()->json('Error', 500);
                }
            } else if ($userType == 'Tenant') {
                try {
                    $userData = DB::table('rent_tenantdetails')->where('tenantid', $userId)->first();
                    $userData->id = $userData->tenantid;
                    return response()->json($userData, 200);
                } catch (Exception $e) {
                    return response()->json('Error', 500);
                }
            }
        }
    }
}
