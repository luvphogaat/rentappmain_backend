<?php
namespace App\Http\Controllers;

use App\Bills;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// use Webpatser\Uuid\Uuid;

class BillsController extends Controller
{
    function list($userid, $roomid) {
        $billList = DB::table('rent_electricitynew')->where('roomid', $roomid)->get();
        // $billList  = DB::table('rent_electricitynew')->get();
        //     return response()->json(Student::orderBy('class', 'desc')->get(), 200);
        return response()->json($billList, 200);
    }

    public function addNewBill(Request $requests)
    {
        $billSave = Bills::create($requests->all());
        return response()->json($billSave, 201);
        // return Uuid::generate()->string;
    }
}
