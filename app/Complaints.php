<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Complaints extends Model
{

    protected $table = 'rent_complaints';

    protected $primaryKey = 'complaintId';

    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [
        'complaintId', 'complaint_message', 'complaint_type', 'complaint_by', 'complaint_status',
    ];

    /**

     * The attributes excluded from the model's JSON form.

     *

     * @var array

     */

    protected $hidden = [];

    public static function boot()
    {

        parent::boot();

        static::creating(function ($model) {

            $model->complaintId = (string) Uuid::generate(4);

        });

    }

    public function getIncrementing()
    {

        return false;

    }

    public function getKeyType()
    {

        return 'string';

    }

}
