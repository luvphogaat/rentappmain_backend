<?php
namespace App\Http\Controllers;

use App\Complaints;
use App\Property;
use App\RentPays;
use App\Room;
use App\SmsRecord;
use App\TenantRoom;
use App\Tenants;
use Aws\Exception\AwsException;
use Aws\Sns\SnsClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use Intervention\Image\Facades\Image as Image;

class TenantController extends Controller
{

    public function search(Request $requests)
    {
        $filterData = array();
        $searchData = DB::table('rent_tenantdetails')->select('tenantid', 'name', 'aadharcard', 'fathername', 'permanentaddress', 'mobilenumber', 'policeverificationStatus', 'profilePicture')
            ->where('aadharcard', $requests->get('searchValue'))->orWhere('mobilenumber', $requests->get('searchValue'))->orWhere('name', 'LIKE', '%' . $requests->get('searchValue') . '%')->get();
        if (count($searchData) > 0) {
            $searchData1 = json_decode($searchData, true)[0]['tenantid'];
            foreach (json_decode($searchData, true) as $key => $value) {
                $tempObject;
                $tenantId = $value['tenantid'];
                $newDataStatus = TenantRoom::where('tenantid', '=', $tenantId)->first() == null ? 'Inactive' : 'Active';
                $tempObject['tenantId'] = $value['tenantid'];
                $tempObject['name'] = $value['name'];
                $tempObject['aadharCardNumber'] = $value['aadharcard'];
                $tempObject['fatherName'] = $value['fathername'];
                $tempObject['permanentAddress'] = $value['permanentaddress'];
                $tempObject['mobileNumber'] = $value['mobilenumber'];
                $tempObject['policeVerificationStatus'] = $value['policeverificationStatus'];
                $tempObject['profilePicture'] = $value['profilePicture'];
                $tempObject['linkStatus'] = $newDataStatus;

                array_push($filterData, $tempObject);

            };
            return response()->json($filterData, 200);
        } else {
            return response()->json($filterData, 200);
        }
    }
    public function tenantlist($ownerid, $propertyid)
    {
        $tenantlist = DB::table('rent_roomdetails')->where('propertyid', $propertyid)->where('ownerid', $ownerid)
            ->orderBy('roomnumber', 'asc')->get();
        //     return response()->json(Student::orderBy('class', 'desc')->get(), 200);
        return response()->json($tenantlist, 200);
    }

    public function roomRecordWithTenant($userid, $roomid)
    {

        try {
            $getLinkedData = TenantRoom::where('roomid', '=', $roomid)->get();
            $roomDetails;
            $tenantDetails;
            $objData;
            $tenantData;
            $propertyDetails;
            $roomDetails = Room::where('roomid', '=', $roomid)->first();
            $propertyDetails = Property::where('propertyid', '=', $roomDetails->propertyid)->first();
            if (count($getLinkedData) == 1) {
                $tenantDetails = Tenants::where('tenantid', '=', $getLinkedData[0]->tenantid)->first();
                $objData['status'] = $getLinkedData[0]->status;
            }
            $objData['propertyid'] = $roomDetails->propertyid;
            $objData['roomid'] = $roomDetails->roomid;
            $objData['rent'] = $roomDetails->rent;
            $objData['security'] = $roomDetails->security;
            $objData['roomtype'] = $roomDetails->roomtype;
            $objData['roomnumber'] = $roomDetails->roomnumber;
            $objData['roomfloor'] = $roomDetails->roomfloor;
            $objData['balcony'] = $roomDetails->balcony;
            $objData['furnished'] = $roomDetails->furnishedStatus;
            $objData['water'] = $roomDetails->watercharges;
            $objData['city'] = $propertyDetails->city;
            $objData['pincode'] = $propertyDetails->pincode;
            $objData['houseNumber'] = $propertyDetails->houseNumber;
            $objData['areaName'] = $propertyDetails->areaName;
            $objData['purpose'] = $roomDetails->purpose;
            $objData['kitchen'] = $roomDetails->kitchen;
            $objData['bathroom'] = $roomDetails->bathroom;
            $objData['electricityType'] = $roomDetails->electricityType;
            $objData['meterType'] = $roomDetails->meterType;
            $objData['roomPictures'] = $roomDetails->images;
            if (count($getLinkedData) > 0) {
                $tenantData['tenantid'] = $tenantDetails->tenantid;
                $tenantData['gender'] = $tenantDetails->gender;
                $tenantData['policeverificationStatus'] = $tenantDetails->policeverificationStatus;
                $tenantData['policeverificationnumber'] = $tenantDetails->policeverificationnumber;
                $tenantData['name'] = $tenantDetails->name;
                $tenantData['profilePicture'] = $tenantDetails->profilePicture;

                $rentPayHistory = RentPays::select('payid', 'status', 'Month_Year', 'pay_date')
                    ->where('roomid', '=', $roomDetails->roomid)->where('tenantId', '=', $tenantDetails->tenantid)->get();
                $tenantData['rentHistory'] = $rentPayHistory;

                $objData['tenant'] = $tenantData;
            }

            return response()->json($objData, 200);

        } catch (Exception $e) {
            return response()->json('error', 500);
        }
    }

    public function saveTenant(Request $request)
    {
        $tenantData = Tenants::create($request->except(['policeVerificationImage','profilePicture']));
        if ($request['profilePicture'] !== '../assets/images/defaultProfile.jpg') {
            $this->uploadProfileFromFunc($tenantData['tenantid'], $request['profilePicture']);
        }
        if ($request['policeVerificationImage']) {
            $uniqIdQ = date('YmdHis');
            $path = '/home/gangaschool/public_html/rent/images/' . $tenantData['tenantid'] . '/';
            $publicPath = 'http://rent.gangarent.in/images/' . $tenantData['tenantid'] . '/';
            if (!is_dir($path)) {
                mkdir($path);
            }
            $image_64 = $request['policeVerificationImage']; //your base64 encoded data
            $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
            $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
            $image = str_replace($replace, '', $image_64);
            $image = str_replace(' ', '+', $image);
            $imageName = 'policeVerify-' . $uniqIdQ . '.' . $extension;
            $putcontent = file_put_contents($path . $imageName, base64_decode($image));
            if ($putcontent) {
                $compressThisImg = $path . $imageName;
                Image::make($compressThisImg)->resize(1024, 768)->save($compressThisImg);
                // array_push($tmpImages, $publicPath . $imageName);
                DB::table('rent_tenantdetails')
                    ->where("rent_tenantdetails.tenantid", '=', $tenantData['tenantid'])
                    ->limit(1)
                    ->update(['rent_tenantdetails.policeVerificationImage' => '[' . $publicPath . $imageName . ']']);
            }
        }
        if ($request['propertyid'] !== 'None') {
            $tenantRoomData = $request->all() + ['tenantid' => $tenantData['tenantid']];
            TenantRoom::create($tenantRoomData);
        }
        return response()->json('Created', 201);
    }

    public function linkTenant(Request $request)
    {
        $data = TenantRoom::create($request->all());
        return response()->json($data, 201);
    }

    public function unlinkTenant($roomid, $tenantid, Request $request)
    {

        try {
            $deleteTenant = TenantRoom::where('roomid', $roomid)->where('tenantid', $tenantid);
            $tobeDelete = $deleteTenant->first();
            $insertInHistory = DB::table('rent_tenantHistory')->insert([
                ['historyId' => (string) Uuid::generate(4),
                    'roomid' => $roomid,
                    'propertyid' => $tobeDelete->propertyid,
                    'tenantid' => $tenantid,
                    'comment' => $request['comment'],
                    'leftOutDate' => date('Y-m-d')],
            ]);
            if ($insertInHistory) {
                $insertInVacant = DB::table('rent_vacantList')->insert([
                    [
                        'vacantId' => (string) Uuid::generate(4),
                        'availablefrom' => $request['availabilityFrom'],
                        'roomid' => $roomid,
                        'propertyid' => $tobeDelete->propertyid,
                    ],
                ]);
                if ($insertInVacant) {
                    $deleteTenant->delete();
                }
            }
            return response()->json('Room Vacated Successfully', 200);
        } catch (Exception $e) {
            return response()->json('Error', 500);
        }
    }

    public function uploadProfile($tenantid, Request $request)
    {
        $uniqIdQ = uniqid();
        $path = '/home/gangaschool/public_html/rent/images/' . $tenantid . '/';
        $publicPath = 'http://rent.gangarent.in/images/' . $tenantid . '/';
        if (!is_dir($path)) {
            mkdir($path, 077, true);
        }
        $image_64 = $request['image']; //your base64 encoded data
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
        $image = str_replace($replace, '', $image_64);
        $image = str_replace(' ', '+', $image);
        $imageName = 'profile-' . $uniqIdQ . '.' . $extension;
        $putcontent = file_put_contents($path . $imageName, base64_decode($image));
        if ($putcontent) {
            DB::table('rent_tenantdetails')
                ->where("rent_tenantdetails.tenantid", '=', $tenantid)
                ->limit(1)
                ->update(['rent_tenantdetails.profilePicture' => $publicPath . $imageName]);
            return response()->json('Uploaded', 200);
        }

    }

    public function uploadProfileFromFunc($tenantid, $objImage)
    {
        $uniqIdQ = uniqid();
        $path = '/home/gangaschool/public_html/rent/images/' . $tenantid . '/';
        $publicPath = 'http://rent.gangarent.in/images/' . $tenantid . '/';
        if (!is_dir($path)) {
            mkdir($path);
        }
        $image_64 = $objImage; //your base64 encoded data
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
        $image = str_replace($replace, '', $image_64);
        $image = str_replace(' ', '+', $image);
        $imageName = 'profile-' . $uniqIdQ . '.' . $extension;
        $putcontent = file_put_contents($path . $imageName, base64_decode($image));
        if ($putcontent) {
            $compressThisImg = $path . $imageName;
            Image::make($compressThisImg)->resize(1024, 768)->save($compressThisImg);
            DB::table('rent_tenantdetails')
                ->where("rent_tenantdetails.tenantid", '=', $tenantid)
                ->limit(1)
                ->update(['rent_tenantdetails.profilePicture' => $publicPath . $imageName]);
        }

    }

    public function uploadPoliceVerification($tenantid, Request $request)
    {
        $uniqIdQ = date('YmdHis');
        $path = '/home/gangaschool/public_html/rent/images/' . $tenantid . '/';
        $publicPath = 'http://rent.gangarent.in/images/' . $tenantid . '/';
        if (!is_dir($path)) {
            mkdir($path, 077, true);
        }
        $image_64 = $request['image']; //your base64 encoded data
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1]; // .jpg .png .pdf
        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
        $image = str_replace($replace, '', $image_64);
        $image = str_replace(' ', '+', $image);
        $imageName = 'policeVerification-' . $uniqIdQ . '.' . $extension;
        $putcontent = file_put_contents($path . $imageName, base64_decode($image));
        // if ($putcontent){
        //     DB::table('rent_tenantdetails')
        //     ->where("rent_tenantdetails.tenantid", '=',  $tenantid)
        //     ->limit(1)
        //     ->update(['rent_tenantdetails.profilePicture'=> $publicPath . $imageName]);
        //     return response()->json('Uploaded', 200);
        // }

    }

    public function profile($tenantid)
    {
        $tempObject;
        $historyArray = array();
        $historyObj;
        $propertyName;
        $roomName;
        $searchData = DB::table('rent_tenantdetails')->where('tenantid', $tenantid)->get();
        if (count($searchData) > 0) {
            foreach (json_decode($searchData, true) as $key => $value) {

                $tenantId = $tenantid;
                $newDataStatus = TenantRoom::where('tenantid', '=', $tenantId)->first() == null ? 'Inactive' : 'Active';
                $tempObject['tenantId'] = $value['tenantid'];
                $tempObject['name'] = $value['name'];
                $tempObject['aadharCardNumber'] = $value['aadharcard'];
                $tempObject['fatherName'] = $value['fathername'];
                $tempObject['permanentAddress'] = $value['permanentaddress'];
                $tempObject['mobileNumber'] = $value['mobilenumber'];
                $tempObject['policeVerificationStatus'] = $value['policeverificationStatus'];
                $tempObject['profilePicture'] = $value['profilePicture'];
                $tempObject['linkStatus'] = $newDataStatus;
                $tempObject['email'] = $value['email'];
                $tempObject['gender'] = $value['gender'];
                $tempObject['dob'] = $value['dob'];
                $tempObject['verificatioNumber'] = $value['policeverificationnumber'];
                $history = DB::table('rent_tenantHistory')->where('tenantid', $tenantId)->get();
                foreach (json_decode($history, true) as $key => $value1) {
                    $propertyName = DB::table('rent_propertydetails')->select('houseNumber', 'areaName', 'city', 'pincode')->where('propertyid', $value1['propertyid'])->first();
                    $roomName = DB::table('rent_roomdetails')->select('roomnumber', 'roomfloor')->where('roomid', $value1['roomid'])->first();
                    $historyObj['lastResidence'] = 'House Number ' . $propertyName->houseNumber . ', ' . $propertyName->areaName . ', ' . $propertyName->city . ' - ' . $propertyName->pincode;
                    $historyObj['residentRoomNumber'] = $roomName->roomnumber;
                    $historyObj['residenceRoomFloor'] = $roomName->roomfloor;
                    $historyObj['leftOutDate'] = $value1['created_at'];
                    array_push($historyArray, $historyObj);
                }
                $tempObject['history'] = $historyArray;

            };
            return response()->json($tempObject, 200);
        }
        // return response()->json($searchData, 200);
    }

    public function profilePicture($tenantid)
    {
        $tenantData = DB::table('rent_tenantdetails')->select('tenantid', 'profilePicture')->where('tenantid', $tenantid)->get();
        return response()->json($tenantData, 200);
    }

    public function dashboard($userId)
    {
        try {
            $complaintStatus = Complaints::where('complaint_by', '=', $userId)->where('complaint_status', '=', 'Pending')->orWhere('complaint_status', '=', 'In-Progress')->get()->count();
            $policeVerificationStatus = Tenants::where('tenantid', '=', $userId)->select('policeverificationStatus')->first();
            $rentStatus = RentPays::select('status')->where('tenantId', '=', $userId)->where('Month_Year', '=', date('M-Y'))->first();
            $jsonData['complaintsCount'] = $complaintStatus;
            $jsonData['policeVerificationStatus'] = $policeVerificationStatus['policeverificationStatus'];
            $jsonData['rentStatus'] = $rentStatus['status'];
            return response()->json($jsonData, 200);
        } catch (Exception $e) {
            return response()->json($e);
        }
    }

    public function sendsmsByOwner(Request $request, $uid)
    {

        $params = array(
            'credentials' => array(
                'key' => 'AKIA3RSEZJD57CJZSYN7',
                'secret' => '7gDnkVeR9B00TwPhD0PvGs1Ttr5udhvgA6YGOZkA',
            ),
            'region' => 'ap-south-1', // < your aws from SNS Topic region
            'version' => 'latest',
        );
        $sns = new \Aws\Sns\SnsClient($params);
        $smsTemplate = '';
        $getLandlordDetails = DB::table('rent_ownerdetails')->select('name', 'mobilenumber')->where('ownerid', $uid)->first();

        switch ($request->input('smsType')) {
            case 'reminder':
                $smsTemplate = "Hi {$request->input('tenantName')}, This is a reminder for the rent of the month - {$request->input('month')}, {$request->input('year')}. If already paid kindly ignore this message." . PHP_EOL
                    . "From Landlord" . PHP_EOL
                    . "Name: {$getLandlordDetails->name}, Contact Number: {$getLandlordDetails->mobilenumber}, Connect for any query" . PHP_EOL
                    . "(sent using Ganga Rent Application)";
                break;
            case 'paymentConfirm':
                $smsTemplate = "Hi {$request->input('tenantName')}, Rent for the month - {$request->input('month')}, {$request->input('year')} received." . PHP_EOL
                    . "From Landlord" . PHP_EOL
                    . "Name: {$getLandlordDetails->name}, Contact Number: {$getLandlordDetails->mobilenumber}, Connect for any query" . PHP_EOL
                    . "(sent using Ganga Rent Application)";
                break;
            default:
                return response()->json('Invalid sms type passed', 400);
        }

        if ($smsTemplate != '') {

            foreach ($request->input('numbers') as $value) {
                try {
                    $args = array(
                        "MessageAttributes" => [
                            'AWS.SNS.SMS.SMSType' => [
                                'DataType' => 'String',
                                'StringValue' => 'Transactional',
                            ],
                        ],
                        "Message" => $smsTemplate,
                        "PhoneNumber" => $value,
                    );

                    $result = $sns->publish($args);
                    if ($result['MessageId']) {
                        try {
                            SmsRecord::firstOrCreate(
                                [
                                    'messageId' => $result['MessageId'],
                                    'message' => $smsTemplate,
                                    'messageFrom' => $uid,
                                    'messageTo' => $value
                                ]
                            );
                            return response()->json([
                                'smsTemplate' => $smsTemplate,
                                'smsResponse' => $result['MessageId'],
                            ], 200);
                        } catch (Exception $e) {
                            return response()->json($e);
                        }
                    }
                } catch (AwsException $e) {
                    // output error message if fails
                    return response()->json($e->getMessage());
                }
            }
        }
    }
}
