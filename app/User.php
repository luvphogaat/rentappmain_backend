<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Webpatser\Uuid\Uuid;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username', 'plan', 'auth_id', 'user_type', 'userId',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public static function boot()
    {

        parent::boot();

        static::creating(function ($model) {

            $model->id = (string) Uuid::generate(4);

        });

    }
}
