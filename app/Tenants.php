<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Tenants extends Model
{

    protected $table = 'rent_tenantdetails';

    protected $primaryKey = 'tenantid';

    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [
        'tenantid', 'name', 'aadharcard', 'fathername', 'permanentaddress', 'mobilenumber', 'alternativenumber', 'email', 'gender', 'dob', 'policeverificationStatus', 'policeverificationnumber', 'status', 'nextpay', 'idProofType', 'idProofNumber', 'policeVerificationImage',
    ];

    /**

     * The attributes excluded from the model's JSON form.

     *

     * @var array

     */

    protected $hidden = [];

    public static function boot()
    {

        parent::boot();

        static::creating(function ($model) {

            $model->tenantid = (string) Uuid::generate(4);

        });

    }

    public function getIncrementing()
    {

        return false;

    }

    public function getKeyType()
    {

        return 'string';

    }

}
