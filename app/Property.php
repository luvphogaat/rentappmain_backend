<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Property extends Model
{

    protected $table = 'rent_propertydetails';

    protected $primaryKey = 'propertyid';

    /**
    tenantid
     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [
        'propertyid', 'ownerid', 'houseNumber', 'addressName', 'areaName', 'city', 'state', 'country', 'floors', 'parking', 'CCTV', 'picture', 'pincode',
    ];

    /**

     * The attributes excluded from the model's JSON form.

     *

     * @var array

     */

    protected $hidden = [];

    public static function boot()
    {

        parent::boot();

        static::creating(function ($model) {

            $model->propertyid = (string) Uuid::generate(4);

        });

    }

    public function getIncrementing()
    {

        return false;

    }

    public function getKeyType()
    {

        return 'string';

    }

}
