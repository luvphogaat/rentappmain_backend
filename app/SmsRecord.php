<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class SmsRecord extends Model
{

    protected $table = 'rent_smsRecords';

    protected $primaryKey = 'id';

    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [
        'id', 'messageId', 'message', 'messageFrom', 'messageTo'
    ];

    /**

     * The attributes excluded from the model's JSON form.

     *

     * @var array

     */

    protected $hidden = [];

    public static function boot()
    {

        parent::boot();

        static::creating(function ($model) {

            $model->id = (string) Uuid::generate(4);

        });

    }

    public function getIncrementing()
    {

        return false;

    }

    public function getKeyType()
    {

        return 'string';

    }

}
